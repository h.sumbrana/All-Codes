### Before
https://prntscr.com/jrbmto

### After
https://prntscr.com/jrbn8l

# Resize Slider to 555px width

```html
<!- Carousel Start Code -->
<script type="text/javascript">
require(["jquery", "owlcarousel"], function($, owl) {
$('.owl-carousel').owlCarousel({
   center: true,
   loop:true,
items: 1,
 singleItem: true,
 nav: true,
navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
})

$(window).resize(function(){
   $('.owl-carousel').trigger('refresh.owl.carousel');
});

});
</script>

<style>
#flexb0424d80-fb4f-43a8-a45c-dd465f60e200 .owl-carousel {
  width: 555px;
  height: 620px;
  display: block;
  margin: auto;
}

#flexb0424d80-fb4f-43a8-a45c-dd465f60e200 .item {
  height: 600px;
  width: 580px;
  padding: 10px;
}

@media (max-width: 979px) {
#flexb0424d80-fb4f-43a8-a45c-dd465f60e200 .owl-carousel {
  width: 100%;
}

#flexb0424d80-fb4f-43a8-a45c-dd465f60e200 .item {
  height: 100%;
}
}
</style>
<!- Carousel End Code -->
```