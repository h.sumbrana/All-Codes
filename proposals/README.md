# Designs

##### Button hover effects
>https://freebiesupply.com/blog/css-button-hover-effects/

##### Awesome Image Hover Pure CSS Part II
>https://codepen.io/maheshambure21/pen/bNXXeM

##### Gradient hover animated button
>https://codepen.io/mars2601/pen/MKVNMX

##### Button.css: CSS3 Button Animations
>https://codepen.io/kevinfan23/pen/BKbWxP

##### Box/Button Hovers
>https://codepen.io/andrewwierzba/pen/JorzzV

##### Parallax Star Background in CSS
>https://codepen.io/saransh/pen/BKJun