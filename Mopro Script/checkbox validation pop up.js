<!--

Short Description: Make a checkbox where in if it is not checked there should be a pop up that will appear and if it is checked it will route to the page.
-->
<style>
div#isNotification > div  {
    width: 60%;
    background: white;
    padding: 30px;
    box-shadow: 0px 4px 2px 0px black;
}
input#isCheckbox {
    height: 100px;
    width: 48px;
}

div#isNotification {
    position: fixed;
    top: -999px;
    z-index: 99999;
    width: 100%;
    transition: 1s;
}

p#isOK {
    margin: 0px;
    width: fit-content;
    padding-left: 30px;
    padding-right: 30px;
    padding-top: 15px;
    padding-bottom: 15px;
    background: #C42627;
    color: white;
    cursor: pointer;
}

@media (max-width: 769px)
{
div#isNotification > div
{
    width: 93% !important;
    box-shadow: 0px 0px 2px 2px black;
}
}
</style>

<script>
document.getElementById("isCheckbox").onchange = function()
{
if(this.checked)
{
document.getElementById("isCreateAccount").href = "https://cloud.sparkmotion.com/#Signup";
document.getElementById("isCreateAccount").removeAttribute("onclick");
}
else
{
document.getElementById("isCreateAccount").href = "javascript:void(0)";
document.getElementById("isCreateAccount").setAttribute("onclick","isCreateAccountNotif()");
}
}

function isCreateAccountNotif()
{
var isElement = document.createElement("div");
isElement.id = "isNotification";
isElement.align = "center";
isElement.innerHTML = '<div><p class="align-center fp-el">You have not certified that you are not a European Union (EU) Citizen. Spark Pro is not compliant with General Data Protection Regulation (GDPR), EU 2016/679, and, therefore, we are not able to provide you with a Spark Pro account unless you certify that you are not a EU Citizen. We are working to become GPDR compliant. Please either certify that you are not a EU Citizen or try again later.</p><p class="align-center fp-el" id="isOK">OK</p></div>';
document.body.appendChild(isElement);
setTimeout(function(){

if(window.screen.width < 770)
{
document.getElementById("isNotification").style = "top:30px";
}
else
{
document.getElementById("isNotification").style = "top:0px";
}

document.getElementById("isOK").onclick = function()
{
document.getElementById("isNotification").style = "";
setTimeout(function(){ document.getElementById("isNotification").remove(); }, 300);
}
},10);


}
</script>