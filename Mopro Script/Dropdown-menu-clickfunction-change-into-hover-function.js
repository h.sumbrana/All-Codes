<!-- 
Builder harold Sumbrana
Description Make a click function into hove function dropdown menu
Approved By Joshua Reganon
 -->
    <script>
require(["jquery"], function($){
 $('ul.nav.nav-pills li#ctl01_rptHeaderMenu_ctl02_liMenuItem').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    }); 


  $('ul.nav.nav-pills li#ctl01_rptHeaderMenu_ctl12_liMenuItem').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    }); 


  $('ul.nav.nav-pills li#ctl01_rptHeaderMenu_ctl21_liMenuItem').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    }); 


});



</script>